use ipnet::{ipv4_mask_to_prefix, IpAddrRange, IpSub, Ipv4Net};
use rand::prelude::*;
use std::{fmt::Debug, io, net::Ipv4Addr, str::FromStr};

fn main() {
    #[derive(Debug, PartialEq)]
    enum IPClasses {
        A,
        B,
        C,
        None,
    }
    impl FromStr for IPClasses {
        type Err = &'static str;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            match s.to_uppercase().as_str() {
                "A" => Ok(IPClasses::A),
                "B" => Ok(IPClasses::B),
                "C" => Ok(IPClasses::C),
                _ => Err("Invalid IP class string"),
            }
        }
    }

    #[derive(Debug)]
    struct Answers {
        q1_cidr: Ipv4Addr,
        // q2_num_of_subnets: usize,
        q3_num_of_hosts_per_subnet: usize,
        q4_first_usable_host: Ipv4Addr,
        q5_last_usable_host: Ipv4Addr,
        q6_subnet_id: Ipv4Addr,
        q7_subnet_broadcast: Ipv4Addr,
    }

    let mut question_count: i32 = 1;
    loop {
        // generate random ip addr
        let mut rng = thread_rng();
        let octet_1: u8 = rng.gen_range(0..223);
        let octet_2: u8 = rng.gen_range(0..255);
        let octet_3: u8 = rng.gen_range(0..255);
        let octet_4: u8 = rng.gen_range(0..255);
        let ip_addess = Ipv4Addr::new(octet_1, octet_2, octet_3, octet_4);

        // generate random subnet mask
        let prefix_len: u8 = rng.gen_range(14..25);

        // ip network
        let ip_net = Ipv4Net::new(ip_addess, prefix_len).unwrap();

        // 0: find the Class of the IP
        let octet_1 = ip_addess.octets()[0];
        let network_class = match &octet_1 {
            0..=127 => IPClasses::A,
            128..=191 => IPClasses::B,
            192..=223 => IPClasses::C,
            _ => IPClasses::A, //TODO not ideal
        };

        // create the correct answers
        // 1: cidr
        let cidr = ip_net.netmask();
        eprintln!("1: {cidr}");

        // //TODO 2: # of subnets
        // let num_of_subnets = match network_class {
        //     IPClasses::A => 2u32.pow(32 - 8 - u32::from(ip_net.prefix_len())),
        //     IPClasses::B => 2u32.pow(32 - 16 - u32::from(ip_net.prefix_len())),
        //     IPClasses::C => 2u32.pow(32 - 16 - u32::from(ip_net.prefix_len())),
        //     _ => 32,
        // };
        // eprintln!("2: nc::{network_class:?} nos::{num_of_subnets} pl::{prefix_len}");

        // 3: # of hosts/subnet
        let num_of_hosts_per_subnet = ip_net.hosts().count();
        eprintln!("3: {num_of_hosts_per_subnet}");

        // 4: first usable host
        let first_usable_host = ip_net.hosts().rev().last().unwrap();
        eprintln!("4: {first_usable_host:?}");

        // 5: last usable host
        let last_usable_host = ip_net.hosts().last().unwrap(); //TODO add better formatting.
        eprintln!("5: {last_usable_host:?}");

        // 6: subnet id
        let subnet_id = ip_net.network();
        eprintln!("6: {subnet_id}");

        // 7: broadcast
        let broadcast = ip_net.broadcast();
        eprintln!("7: {broadcast}");

        let correct_answers = Answers {
            q1_cidr: cidr,
            // q2_num_of_subnets: num_of_subnets,
            q3_num_of_hosts_per_subnet: num_of_hosts_per_subnet,
            q4_first_usable_host: first_usable_host,
            q5_last_usable_host: last_usable_host,
            q6_subnet_id: subnet_id,
            q7_subnet_broadcast: broadcast,
        };

        // show question to the user
        println!("\n\n==============================================");
        println!("Question #{question_count}");
        println!("Using the ip addres of {ip_addess}/{prefix_len} Calculate the\n");
        println!("    Network Class"); //TODO remove anything to do with net class
        println!("   1️⃣ Determine the CIDR notation");
        println!("   2️⃣ Calculate # of Subnets");
        println!("   3️⃣ Calculate  # of Hosts per Subnet");
        println!("   4️⃣ Find the First Usable Host Address of Subnet 1");
        println!("   5️⃣ Find the Last Usable Host Address of Subnet 1");
        println!("   6️⃣ Find the Subnet ID of Subnet 1");
        println!("   7️⃣ Find the Broadcast Address of Subnet 1");
        println!("==============================================");

        // ask for q1
        println!("\n❔1: What is the CIDR?");
        let mut ans1_input: String = String::new();
        match io::stdin().read_line(&mut ans1_input) {
            Ok(_) => ans1_input = String::from(ans1_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans1_ = Ipv4Addr::from_str(&ans1_input).unwrap();

        // ask for q2 network class
        // println!("\n\n==============================================");
        // println!("❔2: What is the Network Class?");
        // let mut ans2_input = String::new();
        // match io::stdin().read_line(&mut ans2_input) {
        //     Ok(_) => {
        //         ans2_input = String::from(ans2_input.trim().to_uppercase());
        //     }
        //     Err(error_input) => eprintln!("{error_input}"),
        // }
        // let ans2_class = IPClasses::from_str(&ans2_input.as_str()).unwrap();

        // ask for q3
        println!("\n❔3: What is the number of hosts per subnet?");
        let mut ans3_input: String = String::new();
        match io::stdin().read_line(&mut ans3_input) {
            Ok(_) => ans3_input = String::from(ans3_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans3_: usize = ans3_input.parse().unwrap();

        // ask for q4
        println!("\n❔4: What is the first usable host?");
        let mut ans4_input: String = String::new();
        match io::stdin().read_line(&mut ans4_input) {
            Ok(_) => ans4_input = String::from(ans4_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans4_ = Ipv4Addr::from_str(&ans4_input).unwrap();

        // ask for q5
        println!("\n❔5: What is the last usable host?");
        let mut ans5_input: String = String::new();
        match io::stdin().read_line(&mut ans5_input) {
            Ok(_) => ans5_input = String::from(ans5_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans5_ = Ipv4Addr::from_str(&ans5_input).unwrap();

        // ask for q6 subnet id
        println!("\n❔6: What is the subnet ID?");
        let mut ans6_input: String = String::new();
        match io::stdin().read_line(&mut ans6_input) {
            Ok(_) => ans6_input = String::from(ans6_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans6_subnet = Ipv4Addr::from_str(&ans6_input).unwrap();

        // ask for q7 broadcast address
        println!("\n❔7: What is the Broadcast address?");
        let mut ans7_input: String = String::new();
        match io::stdin().read_line(&mut ans7_input) {
            Ok(_) => ans7_input = String::from(ans7_input.trim()),
            Err(error) => eprintln!("error {error}"),
        }
        let ans7_broadcast = Ipv4Addr::from_str(&ans7_input).unwrap();

        // create user Answers Struct
        let user_answers = Answers {
            q1_cidr: ans1_,
            // q2_num_of_subnets: ans2_,
            q3_num_of_hosts_per_subnet: ans3_,
            q4_first_usable_host: ans4_,
            q5_last_usable_host: ans5_,
            q6_subnet_id: ans6_subnet,
            q7_subnet_broadcast: ans7_broadcast,
        };

        // check if user's answers are correct
        // check q1
        println!("");
        if user_answers.q1_cidr == correct_answers.q1_cidr {
            println!(
                "✅: {:?} is the correct CIDR for {ip_addess}/{prefix_len}",
                user_answers.q1_cidr
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for the CIDR is {:?}",
                correct_answers.q1_cidr
            )
        }

        // // check q2
        // println!("");
        // if user_answers.q == correct_answers.q {
        //     println!(
        //         "✅: {:?} is the correct N for {ip_addess}/{prefix_len}",
        //         user_answers.q
        //     )
        // } else {
        //     println!("\n❌: You got the wrong answer.");
        //     println!("The correct answer for the N is {:?}", correct_answers.q)
        // }

        // check q3
        println!("");
        if user_answers.q3_num_of_hosts_per_subnet == correct_answers.q3_num_of_hosts_per_subnet {
            println!(
                "✅: {:?} is the correct number of hosts/subnet for {ip_addess}/{prefix_len}",
                user_answers.q3_num_of_hosts_per_subnet
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for hosts/subnet is {:?}",
                correct_answers.q3_num_of_hosts_per_subnet
            )
        }

        // check q4
        println!("");
        if user_answers.q4_first_usable_host == correct_answers.q4_first_usable_host {
            println!(
                "✅: {:?} is the correct first usable host for {ip_addess}/{prefix_len}",
                user_answers.q4_first_usable_host
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for the first usable host is {:?}",
                correct_answers.q4_first_usable_host
            )
        }

        // check q5
        println!("");
        if user_answers.q5_last_usable_host == correct_answers.q5_last_usable_host {
            println!(
                "✅: {:?} is the correct last usable host for {ip_addess}/{prefix_len}",
                user_answers.q5_last_usable_host
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for the last usable host is {:?}",
                correct_answers.q5_last_usable_host
            )
        }

        // check q6 subnet id
        println!("");
        if user_answers.q6_subnet_id == correct_answers.q6_subnet_id {
            println!(
                "✅: {:?} is the correct Subnet ID for {ip_addess}/{prefix_len}",
                user_answers.q6_subnet_id
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for the Subnet ID is {:?}",
                correct_answers.q6_subnet_id
            )
        }

        // check q7 broadcast address
        if user_answers.q7_subnet_broadcast == correct_answers.q7_subnet_broadcast {
            println!(
                "✅: {:?} is the correct  for {ip_addess}/{prefix_len}",
                user_answers.q7_subnet_broadcast
            )
        } else {
            println!("\n❌: You got the wrong answer.");
            println!(
                "The correct answer for the Network Broadcast address is {:?}",
                correct_answers.q7_subnet_broadcast
            )
        }
        println!("\n==============================================");
        question_count += 1;
    }
}
